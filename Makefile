PORT ?= 8888
CONTAINER_NAME ?= ait-docs
PDF_PATH ?= dist/ait_docs.pdf

.PHONY: run
## run: run ait docs website locally using docker
run: generate
	kubectl devops apidocs run --port $(PORT) --container-name $(CONTAINER_NAME)

.PHONY: pdf
## pdf: build ait pdf docs
pdf: generate
	@mkdir -p dist
	kubectl devops apidocs build-pdf --pdf-page-url /pdf --target-file $(PDF_PATH)

.PHONY: genreate
## generate: generate docs from api definition files(swagger, crd)
generate:
	@echo "============> Merging swagger&crd files"
	rm -f apis/versions/2.9.json
	kubectl devops apidocs merge --config=apidocs.yaml --force
	@echo "===========> Generating translation from swagger"
	rm -f apis/en.json
	kubectl devops apidocs translate --config=apidocs.yaml --output-file translations/en.json --force
	@echo "===========> Converting translations to hugo format"
	rm -f i18n/*.json
	kubectl devops apidocs convert --config=apidocs.yaml --input-folder translations --output-folder i18n
	@echo "===========> Generating docs content"
	rm -f data/swagger.json
	kubectl devops apidocs generate --config=apidocs.yaml --data-folder data
	@echo "===========> Removing unused generated contents"
	rm -f content/api/auth/权限视图/apis-auth.alauda.io-v1-views-delete.md
	rm -f content/api/auth/权限视图/apis-auth.alauda.io-v1-views-name-delete.md
	rm -f content/api/auth/权限视图/apis-auth.alauda.io-v1-views-name-patch.md
	rm -f content/api/auth/权限视图/apis-auth.alauda.io-v1-views-name-put.md
	rm -f content/api/auth/权限视图/apis-auth.alauda.io-v1-views-post.md
	rm -f content/api/auth/用户/apis-auth.alauda.io-v1-users-delete.md
	rm -f content/api/auth/用户/apis-auth.alauda.io-v1-users-name-delete.md
	rm -f content/api/auth/用户/apis-auth.alauda.io-v1-users-name-patch.md
	rm -f content/api/auth/用户/apis-auth.alauda.io-v1-users-name-put.md
	rm -f content/api/auth/用户/apis-auth.alauda.io-v1-users-post.md
	rm -f content/api/auth/用户组/apis-auth.alauda.io-v1-groups-delete.md
	rm -f content/api/auth/用户组/apis-auth.alauda.io-v1-groups-name-delete.md
	rm -f content/api/auth/用户组/apis-auth.alauda.io-v1-groups-name-patch.md
	rm -f content/api/auth/用户组/apis-auth.alauda.io-v1-groups-name-put.md
	rm -f content/api/auth/用户组/apis-auth.alauda.io-v1-groups-post.md
	rm -f content/api/cluster/集群仓库/apis-clusterregistry.k8s.io-v1alpha1-namespaces-namespace-clusters-delete.md
	rm -f content/api/cluster/集群仓库/apis-clusterregistry.k8s.io-v1alpha1-namespaces-namespace-clusters-post.md
	rm -f content/api/cluster/集群仓库/apis-clusterregistry.k8s.io-v1alpha1-namespaces-namespace-clusters-name-delete.md
	rm -f content/api/cluster/集群仓库/apis-clusterregistry.k8s.io-v1alpha1-namespaces-namespace-clusters-name-patch.md
	rm -f content/api/cluster/集群仓库/apis-clusterregistry.k8s.io-v1alpha1-namespaces-namespace-clusters-name-put.md
	rm -f content/api/cluster/集群仓库/apis-clusterregistry.k8s.io-v1alpha1-namespaces-namespace-clusters-name-status-patch.md
	rm -f content/api/cluster/集群仓库/apis-clusterregistry.k8s.io-v1alpha1-namespaces-namespace-clusters-name-status-put.md
	rm -f content/api/monitoring/通知信息/apis-aiops.alauda.io-v1beta1-notificationmessages-get.md
	rm -f content/api/monitoring/通知信息/apis-aiops.alauda.io-v1beta1-namespaces-namespace-notificationmessages-delete.md
	rm -f content/api/monitoring/通知信息/apis-aiops.alauda.io-v1beta1-namespaces-namespace-notificationmessages-get.md
	rm -f content/api/monitoring/通知信息/apis-aiops.alauda.io-v1beta1-namespaces-namespace-notificationmessages-name-delete.md
	rm -f content/api/monitoring/通知信息/apis-aiops.alauda.io-v1beta1-namespaces-namespace-notificationmessages-name-get.md
	rm -f content/api/monitoring/通知信息/apis-aiops.alauda.io-v1beta1-namespaces-namespace-notificationmessages-name-patch.md
	rm -f content/api/monitoring/通知信息/apis-aiops.alauda.io-v1beta1-namespaces-namespace-notificationmessages-name-put.md
	rm -f content/api/monitoring/通知信息/apis-aiops.alauda.io-v1beta1-namespaces-namespace-notificationmessages-name-status-get.md
	rm -f content/api/monitoring/通知信息/apis-aiops.alauda.io-v1beta1-namespaces-namespace-notificationmessages-name-status-patch.md
	rm -f content/api/monitoring/通知信息/apis-aiops.alauda.io-v1beta1-namespaces-namespace-notificationmessages-name-status-put.md

.PHONY: help
## help: print help message
help:
	@echo "Usage: "
	@sed -n 's/^##//p' ${MAKEFILE_LIST} | column -t -s ':' |  sed -e 's/^/ /'