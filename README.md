# Alauda 基础架构文档
本仓库用来管理 Alauda 基础架构产品的文档。

[TOC]

## 前置条件
* docker
* kubectl-devops: [安装文档](https://bitbucket.org/mathildetech/hugo-alauda-theme/src/335e7ec7b94f59b88ebfbed5dc5cf24b535cbe0c/CLI.md)

## 项目结构
* `apidocs.yaml`：kubectl-devops 工具的配置文件，定义了如何生成数据文件和文档。具体参见 [apidoc 工具使用文档](https://bitbucket.org/mathildetech/hugo-alauda-theme/src/335e7ec7b94f59b88ebfbed5dc5cf24b535cbe0c/APIDOCS.md)。
* `config.toml`：Hugo 工具的配置文件。
* `apis/`: 存放各个项目的 swagger 和 crd 定义，比如 apis/cluster-registry/crd.yaml 便是 cluster-registry 的 crd 定义。
* `apis/versions/<major.minor>.json`: 该文件是由 kubectl-devops merge 命令根据 apidocs.yaml 配置合并生成的 swagger 文件。详情参见 [apidoc 工具使用文档](https://bitbucket.org/mathildetech/hugo-alauda-theme/src/335e7ec7b94f59b88ebfbed5dc5cf24b535cbe0c/APIDOCS.md)。
* `content/`: 文档的存放位置，文档工程师可以在该目录下放置 markdown 文档，具体规则参见 Hugo 的使用说明。
* `content/api/`: API 文档的存放位置，API 文档是由 kubectl-devops generate 命令自动生成的，不需要手工编写。
* `content/pdf.md`：pdf 页面，用来配置 pdf 文档的下载页面。
* `data/swagger.json`: hugo 的加载的数据文件，由 kubectl-devops generate 命令自动生成，和 apis/versions/<major.minor>.json 内容一致不过将所有 ref 内容转变为了 inline 格式。
* `translation/`: 由 kubectl-devops translate 命令自动生成的翻译文件，用于在 weblate 平台上进行翻译。
* `i18n/`: hugo 使用的国际化翻译文件，是使用 kubectl-devops convert 命令由 translation/ 目录下的文件转换格式而来的。
* `static/`: 文档网站的静态文件和样式文件，文档中使用的图片也放到此处。
* `layout/`: 文档网站的布局文件放在此处。

## 快速入门
* `make run`: 在本地运行文档网站
* `make pdf`：生成 PDF 版本的文档，PDF 文件名为 `ait_docs.pdf`
* `make help`：打印帮助文档