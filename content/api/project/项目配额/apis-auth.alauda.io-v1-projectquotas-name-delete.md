+++
title = "删除指定项目配额"
description = "/apis/auth.alauda.io/v1/projectquotas/{name} DELETE"
weight = 9
path = "DELETE /apis/auth.alauda.io/v1/projectquotas/{name}"
+++


{{%api path="/apis/auth.alauda.io/v1/projectquotas/{name}" verb="DELETE" %}}
