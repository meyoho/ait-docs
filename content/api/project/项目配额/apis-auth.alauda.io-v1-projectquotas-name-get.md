+++
title = "查看项目配额详情"
description = "/apis/auth.alauda.io/v1/projectquotas/{name} GET"
weight = 3
path = "GET /apis/auth.alauda.io/v1/projectquotas/{name}"
+++


{{%api path="/apis/auth.alauda.io/v1/projectquotas/{name}" verb="GET" %}}
