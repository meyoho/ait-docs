+++
title = "查看项目配额列表"
description = "/apis/auth.alauda.io/v1/projectquotas GET"
weight = 2
path = "GET /apis/auth.alauda.io/v1/projectquotas"
+++


{{%api path="/apis/auth.alauda.io/v1/projectquotas" verb="GET" %}}
