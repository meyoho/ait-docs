+++
title = "删除指定项目"
description = "/apis/auth.alauda.io/v1/projects/{name} DELETE"
weight = 9
path = "DELETE /apis/auth.alauda.io/v1/projects/{name}"
+++


{{%api path="/apis/auth.alauda.io/v1/projects/{name}" verb="DELETE" %}}
