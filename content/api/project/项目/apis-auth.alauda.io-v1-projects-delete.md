+++
title = "批量删除项目"
description = "/apis/auth.alauda.io/v1/projects DELETE"
weight = 10
path = "DELETE /apis/auth.alauda.io/v1/projects"
+++


{{%api path="/apis/auth.alauda.io/v1/projects" verb="DELETE" %}}
