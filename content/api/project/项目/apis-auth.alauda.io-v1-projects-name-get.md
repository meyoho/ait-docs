+++
title = "查看项目详情"
description = "/apis/auth.alauda.io/v1/projects/{name} GET"
weight = 3
path = "GET /apis/auth.alauda.io/v1/projects/{name}"
+++


{{%api path="/apis/auth.alauda.io/v1/projects/{name}" verb="GET" %}}
