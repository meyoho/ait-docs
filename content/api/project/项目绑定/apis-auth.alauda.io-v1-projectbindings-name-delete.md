+++
title = "解除指定的项目和集群的绑定关系"
description = "/apis/auth.alauda.io/v1/projectbindings/{name} DELETE"
weight = 6
path = "DELETE /apis/auth.alauda.io/v1/projectbindings/{name}"
+++


{{%api path="/apis/auth.alauda.io/v1/projectbindings/{name}" verb="DELETE" %}}
