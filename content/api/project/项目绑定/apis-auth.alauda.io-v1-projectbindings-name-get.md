+++
title = "查看项目和集群的绑定关系详情"
description = "/apis/auth.alauda.io/v1/projectbindings/{name} GET"
weight = 3
path = "GET /apis/auth.alauda.io/v1/projectbindings/{name}"
+++


{{%api path="/apis/auth.alauda.io/v1/projectbindings/{name}" verb="GET" %}}
