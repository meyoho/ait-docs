+++
title = "查看项目和集群的绑定关系列表"
description = "/apis/auth.alauda.io/v1/projectbindings GET"
weight = 2
path = "GET /apis/auth.alauda.io/v1/projectbindings"
+++


{{%api path="/apis/auth.alauda.io/v1/projectbindings" verb="GET" %}}
