+++
title = "批量删除计量报表"
description = "/apis/metering.alauda.io/v1beta1/reports DELETE"
weight = 10000
path = "DELETE /apis/metering.alauda.io/v1beta1/reports"
+++


{{%api path="/apis/metering.alauda.io/v1beta1/reports" verb="DELETE" %}}
