+++
title = "查看计量报表的状态"
description = "/apis/metering.alauda.io/v1beta1/reports/{name}/status GET"
weight = 60
path = "GET /apis/metering.alauda.io/v1beta1/reports/{name}/status"
+++


{{%api path="/apis/metering.alauda.io/v1beta1/reports/{name}/status" verb="GET" %}}
