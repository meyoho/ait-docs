+++
title = "差异化更新计量报表"
description = "/apis/metering.alauda.io/v1beta1/reports/{name} PATCH"
weight = 50
path = "PATCH /apis/metering.alauda.io/v1beta1/reports/{name}"
+++


{{%api path="/apis/metering.alauda.io/v1beta1/reports/{name}" verb="PATCH" %}}
