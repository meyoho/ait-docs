+++
title = "删除指定的计量报表"
description = "/apis/metering.alauda.io/v1beta1/reports/{name} DELETE"
weight = 90
path = "DELETE /apis/metering.alauda.io/v1beta1/reports/{name}"
+++


{{%api path="/apis/metering.alauda.io/v1beta1/reports/{name}" verb="DELETE" %}}
