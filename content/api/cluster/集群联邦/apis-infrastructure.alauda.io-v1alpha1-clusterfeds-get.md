+++
title = "查看联邦集群列表"
description = "/apis/infrastructure.alauda.io/v1alpha1/clusterfeds GET"
weight = 10
path = "GET /apis/infrastructure.alauda.io/v1alpha1/clusterfeds"
+++


{{%api path="/apis/infrastructure.alauda.io/v1alpha1/clusterfeds" verb="GET" %}}
