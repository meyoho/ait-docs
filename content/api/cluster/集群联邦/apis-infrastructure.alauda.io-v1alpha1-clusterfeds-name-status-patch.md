+++
title = "差异化更新联邦集群的状态"
description = "/apis/infrastructure.alauda.io/v1alpha1/clusterfeds/{name}/status PATCH"
weight = 80
path = "PATCH /apis/infrastructure.alauda.io/v1alpha1/clusterfeds/{name}/status"
+++


{{%api path="/apis/infrastructure.alauda.io/v1alpha1/clusterfeds/{name}/status" verb="PATCH" %}}
