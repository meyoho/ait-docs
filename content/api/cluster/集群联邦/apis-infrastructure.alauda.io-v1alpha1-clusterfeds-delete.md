+++
title = "批量删除联邦集群"
description = "/apis/infrastructure.alauda.io/v1alpha1/clusterfeds DELETE"
weight = 10000
path = "DELETE /apis/infrastructure.alauda.io/v1alpha1/clusterfeds"
+++


{{%api path="/apis/infrastructure.alauda.io/v1alpha1/clusterfeds" verb="DELETE" %}}
