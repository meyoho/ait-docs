+++
title = "监视集群凭证列表"
description = "/apis/platform.tkestack.io/v1/watch/clustercredentials GET"
weight = 60
path = "GET /apis/platform.tkestack.io/v1/watch/clustercredentials"
+++


{{%api path="/apis/platform.tkestack.io/v1/watch/clustercredentials" verb="GET" %}}
