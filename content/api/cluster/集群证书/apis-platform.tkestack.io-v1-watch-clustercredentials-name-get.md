+++
title = "监视指定的集群凭证"
description = "/apis/platform.tkestack.io/v1/watch/clustercredentials/{name} GET"
weight = 70
path = "GET /apis/platform.tkestack.io/v1/watch/clustercredentials/{name}"
+++


{{%api path="/apis/platform.tkestack.io/v1/watch/clustercredentials/{name}" verb="GET" %}}
