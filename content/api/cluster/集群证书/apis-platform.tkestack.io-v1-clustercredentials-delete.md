+++
title = "批量删除集群凭证"
description = "/apis/platform.tkestack.io/v1/clustercredentials DELETE"
weight = 10000
path = "DELETE /apis/platform.tkestack.io/v1/clustercredentials"
+++


{{%api path="/apis/platform.tkestack.io/v1/clustercredentials" verb="DELETE" %}}
