+++
title = "差异化更新主机"
description = "/apis/platform.tkestack.io/v1/machines/{name} PATCH"
weight = 50
path = "PATCH /apis/platform.tkestack.io/v1/machines/{name}"
+++


{{%api path="/apis/platform.tkestack.io/v1/machines/{name}" verb="PATCH" %}}
