+++
title = "差异化更新主机的状态"
description = "/apis/platform.tkestack.io/v1/machines/{name}/status PATCH"
weight = 80
path = "PATCH /apis/platform.tkestack.io/v1/machines/{name}/status"
+++


{{%api path="/apis/platform.tkestack.io/v1/machines/{name}/status" verb="PATCH" %}}
