+++
title = "查看主机列表"
description = "/apis/platform.tkestack.io/v1/machines GET"
weight = 10
path = "GET /apis/platform.tkestack.io/v1/machines"
+++


{{%api path="/apis/platform.tkestack.io/v1/machines" verb="GET" %}}
