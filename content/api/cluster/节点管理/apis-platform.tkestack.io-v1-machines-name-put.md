+++
title = "更新主机"
description = "/apis/platform.tkestack.io/v1/machines/{name} PUT"
weight = 40
path = "PUT /apis/platform.tkestack.io/v1/machines/{name}"
+++


{{%api path="/apis/platform.tkestack.io/v1/machines/{name}" verb="PUT" %}}
