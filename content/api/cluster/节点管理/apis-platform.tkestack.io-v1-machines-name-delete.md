+++
title = "删除指定的主机"
description = "/apis/platform.tkestack.io/v1/machines/{name} DELETE"
weight = 150
path = "DELETE /apis/platform.tkestack.io/v1/machines/{name}"
+++


{{%api path="/apis/platform.tkestack.io/v1/machines/{name}" verb="DELETE" %}}
