+++
title = "查看主机的终结器"
description = "/apis/platform.tkestack.io/v1/machines/{name}/finalize GET"
weight = 110
path = "GET /apis/platform.tkestack.io/v1/machines/{name}/finalize"
+++


{{%api path="/apis/platform.tkestack.io/v1/machines/{name}/finalize" verb="GET" %}}
