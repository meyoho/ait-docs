+++
title = "批量删除主机"
description = "/apis/platform.tkestack.io/v1/machines DELETE"
weight = 10000
path = "DELETE /apis/platform.tkestack.io/v1/machines"
+++


{{%api path="/apis/platform.tkestack.io/v1/machines" verb="DELETE" %}}
