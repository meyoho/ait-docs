+++
title = "更新主机的终结器"
description = "/apis/platform.tkestack.io/v1/machines/{name}/finalize PUT"
weight = 120
path = "PUT /apis/platform.tkestack.io/v1/machines/{name}/finalize"
+++


{{%api path="/apis/platform.tkestack.io/v1/machines/{name}/finalize" verb="PUT" %}}
