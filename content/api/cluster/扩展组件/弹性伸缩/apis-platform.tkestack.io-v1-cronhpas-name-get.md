+++
title = "查看 CronHPA 详情"
description = "/apis/platform.tkestack.io/v1/cronhpas/{name} GET"
weight = 30
path = "GET /apis/platform.tkestack.io/v1/cronhpas/{name}"
+++


{{%api path="/apis/platform.tkestack.io/v1/cronhpas/{name}" verb="GET" %}}
