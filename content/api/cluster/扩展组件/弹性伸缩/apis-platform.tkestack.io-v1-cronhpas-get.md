+++
title = "查看 CronHPA 列表"
description = "/apis/platform.tkestack.io/v1/cronhpas GET"
weight = 10
path = "GET /apis/platform.tkestack.io/v1/cronhpas"
+++


{{%api path="/apis/platform.tkestack.io/v1/cronhpas" verb="GET" %}}
