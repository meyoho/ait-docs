+++
title = "差异化更新 CronHPA"
description = "/apis/platform.tkestack.io/v1/cronhpas/{name} PATCH"
weight = 50
path = "PATCH /apis/platform.tkestack.io/v1/cronhpas/{name}"
+++


{{%api path="/apis/platform.tkestack.io/v1/cronhpas/{name}" verb="PATCH" %}}
