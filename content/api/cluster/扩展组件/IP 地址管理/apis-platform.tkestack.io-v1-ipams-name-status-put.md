+++
title = "更新 IPAM 的状态"
description = "/apis/platform.tkestack.io/v1/ipams/{name}/status PUT"
weight = 70
path = "PUT /apis/platform.tkestack.io/v1/ipams/{name}/status"
+++


{{%api path="/apis/platform.tkestack.io/v1/ipams/{name}/status" verb="PUT" %}}
