+++
title = "删除指定的 IPAM"
description = "/apis/platform.tkestack.io/v1/ipams/{name} DELETE"
weight = 120
path = "DELETE /apis/platform.tkestack.io/v1/ipams/{name}"
+++


{{%api path="/apis/platform.tkestack.io/v1/ipams/{name}" verb="DELETE" %}}
