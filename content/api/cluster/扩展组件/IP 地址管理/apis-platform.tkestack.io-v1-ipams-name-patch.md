+++
title = "差异化更新 IPAM"
description = "/apis/platform.tkestack.io/v1/ipams/{name} PATCH"
weight = 50
path = "PATCH /apis/platform.tkestack.io/v1/ipams/{name}"
+++


{{%api path="/apis/platform.tkestack.io/v1/ipams/{name}" verb="PATCH" %}}
