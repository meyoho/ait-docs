+++
title = "差异化更新 IPAM 的状态"
description = "/apis/platform.tkestack.io/v1/ipams/{name}/status PATCH"
weight = 80
path = "PATCH /apis/platform.tkestack.io/v1/ipams/{name}/status"
+++


{{%api path="/apis/platform.tkestack.io/v1/ipams/{name}/status" verb="PATCH" %}}
