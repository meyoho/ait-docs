+++
title = "查看 TappController 详情"
description = "/apis/platform.tkestack.io/v1/tappcontrollers/{name} GET"
weight = 30
path = "GET /apis/platform.tkestack.io/v1/tappcontrollers/{name}"
+++


{{%api path="/apis/platform.tkestack.io/v1/tappcontrollers/{name}" verb="GET" %}}
