+++
title = "更新 TappController"
description = "/apis/platform.tkestack.io/v1/tappcontrollers/{name} PUT"
weight = 40
path = "PUT /apis/platform.tkestack.io/v1/tappcontrollers/{name}"
+++


{{%api path="/apis/platform.tkestack.io/v1/tappcontrollers/{name}" verb="PUT" %}}
