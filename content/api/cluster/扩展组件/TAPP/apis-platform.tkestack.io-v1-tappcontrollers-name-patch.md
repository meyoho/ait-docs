+++
title = "差异化更新 TappController"
description = "/apis/platform.tkestack.io/v1/tappcontrollers/{name} PATCH"
weight = 50
path = "PATCH /apis/platform.tkestack.io/v1/tappcontrollers/{name}"
+++


{{%api path="/apis/platform.tkestack.io/v1/tappcontrollers/{name}" verb="PATCH" %}}
