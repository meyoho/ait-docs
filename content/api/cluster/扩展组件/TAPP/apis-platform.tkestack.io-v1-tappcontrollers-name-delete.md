+++
title = "删除指定的 TappController"
description = "/apis/platform.tkestack.io/v1/tappcontrollers/{name} DELETE"
weight = 120
path = "DELETE /apis/platform.tkestack.io/v1/tappcontrollers/{name}"
+++


{{%api path="/apis/platform.tkestack.io/v1/tappcontrollers/{name}" verb="DELETE" %}}
