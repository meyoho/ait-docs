+++
title = "查看 GPUManager 详情"
description = "/apis/platform.tkestack.io/v1/gpumanagers/{name} GET"
weight = 30
path = "GET /apis/platform.tkestack.io/v1/gpumanagers/{name}"
+++


{{%api path="/apis/platform.tkestack.io/v1/gpumanagers/{name}" verb="GET" %}}
