+++
title = "监视 GPUManager 列表"
description = "/apis/platform.tkestack.io/v1/watch/gpumanagers GET"
weight = 90
path = "GET /apis/platform.tkestack.io/v1/watch/gpumanagers"
+++


{{%api path="/apis/platform.tkestack.io/v1/watch/gpumanagers" verb="GET" %}}
