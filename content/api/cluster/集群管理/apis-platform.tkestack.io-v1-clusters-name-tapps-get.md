+++
title = "查看 TappController 扩展组件详情"
description = "/apis/platform.tkestack.io/v1/clusters/{name}/tapps GET"
weight = 410
path = "GET /apis/platform.tkestack.io/v1/clusters/{name}/tapps"
+++


{{%api path="/apis/platform.tkestack.io/v1/clusters/{name}/tapps" verb="GET" %}}
