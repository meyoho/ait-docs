+++
title = "查看集群的状态"
description = "/apis/platform.tkestack.io/v1/clusters/{name}/status GET"
weight = 60
path = "GET /apis/platform.tkestack.io/v1/clusters/{name}/status"
+++


{{%api path="/apis/platform.tkestack.io/v1/clusters/{name}/status" verb="GET" %}}
