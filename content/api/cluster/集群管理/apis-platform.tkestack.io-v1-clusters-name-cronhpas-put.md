+++
title = "更新 CronHPA 扩展组件"
description = "/apis/platform.tkestack.io/v1/clusters/{name}/cronhpas PUT"
weight = 310
path = "PUT /apis/platform.tkestack.io/v1/clusters/{name}/cronhpas"
+++


{{%api path="/apis/platform.tkestack.io/v1/clusters/{name}/cronhpas" verb="PUT" %}}
