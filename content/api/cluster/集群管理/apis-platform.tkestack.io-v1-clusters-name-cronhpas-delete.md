+++
title = "删除 CronHPA 扩展组件"
description = "/apis/platform.tkestack.io/v1/clusters/{name}/cronhpas DELETE"
weight = 330
path = "DELETE /apis/platform.tkestack.io/v1/clusters/{name}/cronhpas"
+++


{{%api path="/apis/platform.tkestack.io/v1/clusters/{name}/cronhpas" verb="DELETE" %}}
