+++
title = "差异化更新 TappController 扩展组件"
description = "/apis/platform.tkestack.io/v1/clusters/{name}/tapps PATCH"
weight = 430
path = "PATCH /apis/platform.tkestack.io/v1/clusters/{name}/tapps"
+++


{{%api path="/apis/platform.tkestack.io/v1/clusters/{name}/tapps" verb="PATCH" %}}
