+++
title = "更新集群"
description = "/apis/platform.tkestack.io/v1/clusters/{name} PUT"
weight = 40
path = "PUT /apis/platform.tkestack.io/v1/clusters/{name}"
+++


{{%api path="/apis/platform.tkestack.io/v1/clusters/{name}" verb="PUT" %}}
