+++
title = "删除 TappController 扩展组件"
description = "/apis/platform.tkestack.io/v1/clusters/{name}/tapps DELETE"
weight = 440
path = "DELETE /apis/platform.tkestack.io/v1/clusters/{name}/tapps"
+++


{{%api path="/apis/platform.tkestack.io/v1/clusters/{name}/tapps" verb="DELETE" %}}
