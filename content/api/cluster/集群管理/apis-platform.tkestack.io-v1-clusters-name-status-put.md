+++
title = "更新集群的状态"
description = "/apis/platform.tkestack.io/v1/clusters/{name}/status PUT"
weight = 70
path = "PUT /apis/platform.tkestack.io/v1/clusters/{name}/status"
+++


{{%api path="/apis/platform.tkestack.io/v1/clusters/{name}/status" verb="PUT" %}}
