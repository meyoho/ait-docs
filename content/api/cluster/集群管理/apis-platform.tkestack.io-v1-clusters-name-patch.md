+++
title = "差异化更新集群"
description = "/apis/platform.tkestack.io/v1/clusters/{name} PATCH"
weight = 50
path = "PATCH /apis/platform.tkestack.io/v1/clusters/{name}"
+++


{{%api path="/apis/platform.tkestack.io/v1/clusters/{name}" verb="PATCH" %}}
