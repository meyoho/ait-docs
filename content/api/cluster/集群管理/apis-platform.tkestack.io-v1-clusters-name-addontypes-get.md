+++
title = "查看指定集群的插件类型"
description = "/apis/platform.tkestack.io/v1/clusters/{name}/addontypes GET"
weight = 200
path = "GET /apis/platform.tkestack.io/v1/clusters/{name}/addontypes"
+++


{{%api path="/apis/platform.tkestack.io/v1/clusters/{name}/addontypes" verb="GET" %}}
