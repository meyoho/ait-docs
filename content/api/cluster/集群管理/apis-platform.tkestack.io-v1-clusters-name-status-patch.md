+++
title = "差异化更新集群的状态"
description = "/apis/platform.tkestack.io/v1/clusters/{name}/status PATCH"
weight = 80
path = "PATCH /apis/platform.tkestack.io/v1/clusters/{name}/status"
+++


{{%api path="/apis/platform.tkestack.io/v1/clusters/{name}/status" verb="PATCH" %}}
