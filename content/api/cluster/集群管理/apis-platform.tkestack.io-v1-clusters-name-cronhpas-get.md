+++
title = "查看 CronHPA 扩展组件详情"
description = "/apis/platform.tkestack.io/v1/clusters/{name}/cronhpas GET"
weight = 305
path = "GET /apis/platform.tkestack.io/v1/clusters/{name}/cronhpas"
+++


{{%api path="/apis/platform.tkestack.io/v1/clusters/{name}/cronhpas" verb="GET" %}}
