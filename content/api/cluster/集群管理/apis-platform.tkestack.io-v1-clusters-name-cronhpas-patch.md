+++
title = "差异化更新 CronHPA 扩展组件"
description = "/apis/platform.tkestack.io/v1/clusters/{name}/cronhpas PATCH"
weight = 320
path = "PATCH /apis/platform.tkestack.io/v1/clusters/{name}/cronhpas"
+++


{{%api path="/apis/platform.tkestack.io/v1/clusters/{name}/cronhpas" verb="PATCH" %}}
