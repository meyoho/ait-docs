+++
title = "查看集群插件类型列表"
description = "/apis/platform.tkestack.io/v1/clusteraddontypes GET"
weight = 10
path = "GET /apis/platform.tkestack.io/v1/clusteraddontypes"
+++


{{%api path="/apis/platform.tkestack.io/v1/clusteraddontypes" verb="GET" %}}
