+++
title = "查看指定的集群插件类型的名称"
description = "/apis/platform.tkestack.io/v1/clusteraddontypes/{name} GET"
weight = 20
path = "GET /apis/platform.tkestack.io/v1/clusteraddontypes/{name}"
+++


{{%api path="/apis/platform.tkestack.io/v1/clusteraddontypes/{name}" verb="GET" %}}
