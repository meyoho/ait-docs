+++
title = "查看集群列表"
description = "/apis/clusterregistry.k8s.io/v1alpha1/clusters GET"
weight = 2
path = "GET /apis/clusterregistry.k8s.io/v1alpha1/clusters"
+++


{{%api path="/apis/clusterregistry.k8s.io/v1alpha1/clusters" verb="GET" %}}
