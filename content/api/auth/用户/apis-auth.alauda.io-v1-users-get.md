+++
title = "查看用户列表"
description = "/apis/auth.alauda.io/v1/users GET"
weight = 2
path = "GET /apis/auth.alauda.io/v1/users"
+++


{{%api path="/apis/auth.alauda.io/v1/users" verb="GET" %}}
