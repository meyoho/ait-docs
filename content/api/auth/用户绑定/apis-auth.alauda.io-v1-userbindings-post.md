+++
title = "为用户绑定角色"
description = "/apis/auth.alauda.io/v1/userbindings POST"
weight = 1
path = "POST /apis/auth.alauda.io/v1/userbindings"
+++


{{%api path="/apis/auth.alauda.io/v1/userbindings" verb="POST" %}}
