+++
title = "更新用户和用户组的绑定关系信息"
description = "/apis/auth.alauda.io/v1/groupbindings/{name} PUT"
weight = 5
path = "PUT /apis/auth.alauda.io/v1/groupbindings/{name}"
+++


{{%api path="/apis/auth.alauda.io/v1/groupbindings/{name}" verb="PUT" %}}
