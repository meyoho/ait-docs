+++
title = "查看权限视图详情"
description = "/apis/auth.alauda.io/v1/views/{name} GET"
weight = 3
path = "GET /apis/auth.alauda.io/v1/views/{name}"
+++


{{%api path="/apis/auth.alauda.io/v1/views/{name}" verb="GET" %}}
