+++
title = "更新 Prometheus 告警规则"
description = "/apis/monitoring.coreos.com/v1/namespaces/{namespace}/prometheusrules/{name} PUT"
weight = 6
path = "PUT /apis/monitoring.coreos.com/v1/namespaces/{namespace}/prometheusrules/{name}"
+++


{{%api path="/apis/monitoring.coreos.com/v1/namespaces/{namespace}/prometheusrules/{name}" verb="PUT" %}}
