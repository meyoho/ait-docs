+++
title = "删除指定 Prometheus 告警规则"
description = "/apis/monitoring.coreos.com/v1/namespaces/{namespace}/prometheusrules/{name} DELETE"
weight = 7
path = "DELETE /apis/monitoring.coreos.com/v1/namespaces/{namespace}/prometheusrules/{name}"
+++


{{%api path="/apis/monitoring.coreos.com/v1/namespaces/{namespace}/prometheusrules/{name}" verb="DELETE" %}}
