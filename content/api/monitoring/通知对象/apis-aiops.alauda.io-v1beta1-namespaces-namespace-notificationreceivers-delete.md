+++
title = "批量删除通知对象"
description = "/apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notificationreceivers DELETE"
weight = 10000
path = "DELETE /apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notificationreceivers"
+++


{{%api path="/apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notificationreceivers" verb="DELETE" %}}
