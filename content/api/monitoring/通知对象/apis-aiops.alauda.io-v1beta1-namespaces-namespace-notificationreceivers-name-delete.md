+++
title = "删除指定的通知对象"
description = "/apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notificationreceivers/{name} DELETE"
weight = 100
path = "DELETE /apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notificationreceivers/{name}"
+++


{{%api path="/apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notificationreceivers/{name}" verb="DELETE" %}}
