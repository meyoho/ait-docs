+++
title = "查看指定项目下的通知列表"
description = "/apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notifications GET"
weight = 2
path = "GET /apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notifications"
+++


{{%api path="/apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notifications" verb="GET" %}}
