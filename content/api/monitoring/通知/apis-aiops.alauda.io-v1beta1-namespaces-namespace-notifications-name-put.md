+++
title = "更新通知"
description = "/apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notifications/{name} PUT"
weight = 6
path = "PUT /apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notifications/{name}"
+++


{{%api path="/apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notifications/{name}" verb="PUT" %}}
