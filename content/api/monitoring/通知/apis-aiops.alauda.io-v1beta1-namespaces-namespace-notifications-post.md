+++
title = "创建通知"
description = "/apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notifications POST"
weight = 1
path = "POST /apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notifications"
+++


{{%api path="/apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notifications" verb="POST" %}}
