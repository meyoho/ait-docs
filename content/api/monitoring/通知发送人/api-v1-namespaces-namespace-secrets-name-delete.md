+++
title = "删除指定的通知发送人"
description = "/api/v1/namespaces/{namespace}/secrets/{name} DELETE"
weight = 70
path = "DELETE /api/v1/namespaces/{namespace}/secrets/{name}"
+++


{{%api path="/api/v1/namespaces/{namespace}/secrets/{name}" verb="DELETE" %}}
