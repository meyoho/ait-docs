+++
title = "差异化更新通知发送人"
description = "/api/v1/namespaces/{namespace}/secrets/{name} PATCH"
weight = 50
path = "PATCH /api/v1/namespaces/{namespace}/secrets/{name}"
+++


{{%api path="/api/v1/namespaces/{namespace}/secrets/{name}" verb="PATCH" %}}
