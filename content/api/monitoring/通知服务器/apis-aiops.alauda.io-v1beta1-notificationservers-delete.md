+++
title = "批量删除通知服务器"
description = "/apis/aiops.alauda.io/v1beta1/notificationservers DELETE"
weight = 10000
path = "DELETE /apis/aiops.alauda.io/v1beta1/notificationservers"
+++


{{%api path="/apis/aiops.alauda.io/v1beta1/notificationservers" verb="DELETE" %}}
