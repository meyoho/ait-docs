+++
title = "更新通知服务器"
description = "/apis/aiops.alauda.io/v1beta1/notificationservers/{name} PUT"
weight = 40
path = "PUT /apis/aiops.alauda.io/v1beta1/notificationservers/{name}"
+++


{{%api path="/apis/aiops.alauda.io/v1beta1/notificationservers/{name}" verb="PUT" %}}
