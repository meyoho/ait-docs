+++
title = "差异化更新通知服务器"
description = "/apis/aiops.alauda.io/v1beta1/notificationservers/{name} PATCH"
weight = 50
path = "PATCH /apis/aiops.alauda.io/v1beta1/notificationservers/{name}"
+++


{{%api path="/apis/aiops.alauda.io/v1beta1/notificationservers/{name}" verb="PATCH" %}}
