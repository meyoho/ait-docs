+++
title = "查看通知模板列表"
description = "/apis/aiops.alauda.io/v1beta1/notificationtemplates GET"
weight = 2
path = "GET /apis/aiops.alauda.io/v1beta1/notificationtemplates"
+++


{{%api path="/apis/aiops.alauda.io/v1beta1/notificationtemplates" verb="GET" %}}
