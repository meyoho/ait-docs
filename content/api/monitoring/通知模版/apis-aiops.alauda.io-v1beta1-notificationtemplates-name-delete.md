+++
title = "删除指定通知模板"
description = "/apis/aiops.alauda.io/v1beta1/notificationtemplates/{name} DELETE"
weight = 6
path = "DELETE /apis/aiops.alauda.io/v1beta1/notificationtemplates/{name}"
+++


{{%api path="/apis/aiops.alauda.io/v1beta1/notificationtemplates/{name}" verb="DELETE" %}}
