+++
title = "更新通知模板"
description = "/apis/aiops.alauda.io/v1beta1/notificationtemplates/{name} PUT"
weight = 5
path = "PUT /apis/aiops.alauda.io/v1beta1/notificationtemplates/{name}"
+++


{{%api path="/apis/aiops.alauda.io/v1beta1/notificationtemplates/{name}" verb="PUT" %}}
